// -*- C++ -*-
//
// MC4BSMAnalysis.h is a part of Herwig++ - A multi-purpose Monte Carlo event generator
// Copyright (C) 2002-2007 The Herwig Collaboration
//
// Herwig++ is licenced under version 2 of the GPL, see COPYING for details.
// Please respect the MCnet academic guidelines, see GUIDELINES for details.
//
#ifndef HERWIG_HwSimpleAnalysis_H
#define HERWIG_HwSimpleAnalysis_H
//
// This is the declaration of the HwSimpleAnalysis class.
//

#include "ThePEG/Repository/CurrentGenerator.h"
#include "ThePEG/Handlers/AnalysisHandler.h"
#include "ThePEG/Config/Pointers.h"
#include "Herwig/Utilities/Histogram.h"

#include <cmath>
#include <fstream>

//Fastjet headers
#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/tools/MassDropTagger.hh"
#include "fastjet/tools/Filter.hh"
#include "fastjet/ClusterSequenceArea.hh"
#include <fastjet/tools/JHTopTagger.hh>
#include <fastjet/Selector.hh>

//Boost headers
#include <boost/algorithm/string.hpp>
#include <boost/tuple/tuple.hpp>

namespace Herwig {

class HwSimpleAnalysis;

}

namespace ThePEG {

ThePEG_DECLARE_POINTERS(Herwig::HwSimpleAnalysis,HwSimpleAnalysisPtr);

}


namespace Herwig {

  using namespace ThePEG;
  using namespace std;
  using namespace fastjet;
  
/**
 * The HwSimpleAnalysis class is designed to perform some simple analysis
 *
 * @see \ref HwSimpleAnalysisInterfaces "The interfaces"
 * defined for HwSimpleAnalysis.
 */
class HwSimpleAnalysis: public AnalysisHandler {

public:

  /**
   * The default constructor.
   */
  inline HwSimpleAnalysis();


  /** @name Functions used by the persistent I/O system. */
  //@{
  /**
   * Function used to write out object persistently.
   * @param os the persistent output stream written to.
   */
  void persistentOutput(PersistentOStream & os) const;

  /**
   * Function used to read in object persistently.
   * @param is the persistent input stream read from.
   * @param version the version number of the object when written.
   */
  void persistentInput(PersistentIStream & is, int version);
  //@}
  
  /** @name Virtual functions required by the AnalysisHandler class. */
  //@{
  /**
   * Analyze a given Event. Note that a fully generated event
   * may be presented several times, if it has been manipulated in
   * between. The default version of this function will call transform
   * to make a lorentz transformation of the whole event, then extract
   * all final state particles and call analyze(tPVector) of this
   * analysis object and those of all associated analysis objects. The
   * default version will not, however, do anything on events which
   * have not been fully generated, or have been manipulated in any
   * way.
   * @param event pointer to the Event to be analyzed.
   * @param ieve the event number.
   * @param loop the number of times this event has been presented.
   * If negative the event is now fully generated.
   * @param state a number different from zero if the event has been
   * manipulated in some way since it was last presented.
   */
  virtual void analyze(tEventPtr event, long ieve, int loop, int state);

 
  //@}


public:

  /**
   * The standard Init function used to initialize the interfaces.
   * Called exactly once for each class by the class description system
   * before the main function starts or
   * when this class is dynamically loaded.
   */
  static void Init();



protected:

  /** @name Clone Methods. */
  //@{
  /**
   * Make a simple clone of this object.
   * @return a pointer to the new object.
   */
  inline virtual IBPtr clone() const;

  /** Make a clone of this object, possibly modifying the cloned object
   * to make it sane.
   * @return a pointer to the new object.
   */
  inline virtual IBPtr fullclone() const;
  //@}



protected:
  inline virtual void doinitrun();
  /** @name Standard Interfaced functions. */
  //@{
  /**
   * Finalize this object. Called in the run phase just after a
   * run has ended. Used eg. to write out statistics.
   */
  virtual void dofinish();
  //@}

private:

  /**
   * The static object used to initialize the description of this class.
   * Indicates that this is a concrete class with persistent data.
   */
  static ClassDescription<HwSimpleAnalysis> initHwSimpleAnalysis;

  /**
   * The assignment operator is private and must never be called.
   * In fact, it should not even be implemented.
   */
  HwSimpleAnalysis & operator=(const HwSimpleAnalysis &);
  
  /** particle information in the order: 
   * 4 momenta (E,x,y,z), id, boolean daughter of Higgs, boolean daughter of b-quark, unused
   **/
  double objects[8][10000];

  /* 
   * the event weight
   */ 
  double evweight;
 
  /* 
   * total number of events.
   */ 
  double numevents_;

   //total number of particles in an event
  int numparticles;


private:

  /**
   *  ONLY the final-state particles that are supposed to go for jet clustering.
   */
  tPVector particlesToCluster_;
  
  /*
   * The jet algorithm used for parton-jet matching in the HH procedure.
   */
  int jetAlgorithm_;
  
  /* global particle cuts */
  double cut_eta_;
  double cut_pt_part_;
  
  /* jet cuts */
  double cut_pt_jet_; //pt cut for jets
  double cut_eta_jet_; //pseudo-rapidity cut for jets

  /*
   * jet algorithm 
   */
  double R_;

    /*
   * electron isolation radius
   */
  double electronIsoR_;  

};

}

#include "ThePEG/Utilities/ClassTraits.h"

namespace ThePEG {

/** @cond TRAITSPECIALIZATIONS */

/** This template specialization informs ThePEG about the
 *  base classes of HwSimpleAnalysis. */
template <>
struct BaseClassTrait<Herwig::HwSimpleAnalysis,1> {
  /** Typedef of the first base class of HwSimpleAnalysis. */
  typedef AnalysisHandler NthBase;
};

/** This template specialization informs ThePEG about the name of
 *  the HwSimpleAnalysis class and the shared object where it is defined. */
template <>
struct ClassTraits<Herwig::HwSimpleAnalysis>
  : public ClassTraitsBase<Herwig::HwSimpleAnalysis> {
  /** Return a platform-independent class name */
  static string className() { return "Herwig::HwSimpleAnalysis"; }
  /** Return the name(s) of the shared library (or libraries) be loaded to get
   *  access to the HwSimpleAnalysis class and any other class on which it depends
   *  (except the base class). */
  static string library() { return "HwSimpleAnalysis.so"; }
};

/** @endcond */

}

#endif /* HERWIG_HwSimpleAnalysis_H */
